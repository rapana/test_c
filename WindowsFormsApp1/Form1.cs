﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user_path = this.textBox1.Text;
            //string PathFile = @"C:\tmp\НТЦ\короткая версия\2026 лето_мин (сечение) -Вариант 1.rg2";
            string PathFile = user_path;
            //Создание указателя на экземпляр RastrWin и его запуск
            ASTRALib.IRastr Rastr = new ASTRALib.Rastr();
            //Загрузка файла с данными 
            Rastr.Load(ASTRALib.RG_KOD.RG_REPL, PathFile, "");
            //Объявление объекта, содержащего таблицу "Узлы"
            // ASTRALib.ITable Node = Rastr.Tables.Item("node");
            ASTRALib.ITable Vetv = Rastr.Tables.Item("vetv");
            Vetv.SetSel("sel");
            int i = Vetv.get_FindNextSel(-1);
            ASTRALib.ICol colName = Vetv.Cols.Item("name");
            string name = colName.get_ZN(i);
            MessageBox.Show(name);
            //Создание объектов, содержащих информацию по каждой колонке
            /* ASTRALib.ICol numberBus = Node.Cols.Item("ny"); //Номер Узла
             ASTRALib.ICol powerActiveLoad = Node.Cols.Item("pn"); //активная мощность нагрузки.
             ASTRALib.ICol powerRectiveLoad = Node.Cols.Item("qn"); //реактивная мощность нагрузки.
             ASTRALib.ICol voltageCalc = Node.Cols.Item("vras"); //Расчётное напряжение.*/
        }
    }
}
